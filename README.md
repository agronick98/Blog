# Blog

A fully functional Blog created with the Django and Bootstrap frameworks.  
Live version: https://agronick.pythonanywhere.com/

To deploy [in production](https://www.youtube.com/watch?v=Sa_kQheCnds&list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p&index=13):
- ensure DEBUG=False in settings.py
- run `manage.py collectstatic` to update static files
- setup static file mapping
  - pythonanywhere:
    - /static/
      - /home/agronick/Blog/blog/static
    - /media/
      - /home/agronick/Blog/media
    - /static/admin/
      - /home/agronick/.virtualenvs/django-blog/lib/python3.9/site-packages/django/contrib/admin/static/admin
  - Fix gmail alert not working for password reset [here](https://stackoverflow.com/a/26698173/12497679)
